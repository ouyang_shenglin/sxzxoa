-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.7.28 - MySQL Community Server (GPL)
-- 服务器OS:                        Win64
-- HeidiSQL 版本:                  10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for labmanagement
CREATE DATABASE IF NOT EXISTS `labmanagement` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `labmanagement`;

-- Dumping structure for table labmanagement.approval
CREATE TABLE IF NOT EXISTS `approval` (
  `approval_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '审批id',
  `form_id` varchar(20) NOT NULL COMMENT '申请表id',
  `status` int(20) NOT NULL COMMENT '状态',
  `created_at` timestamp NOT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`approval_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.approval: ~3 rows (大约)
/*!40000 ALTER TABLE `approval` DISABLE KEYS */;
INSERT INTO `approval` (`approval_id`, `form_id`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'A2009225256', 1, '2020-09-22 20:43:16', NULL),
	(2, 'A2009225257', 1, '2020-09-23 10:59:14', NULL),
	(3, 'C2010225258', 1, '2020-09-23 17:42:16', '2020-09-24 08:16:25');
/*!40000 ALTER TABLE `approval` ENABLE KEYS */;

-- Dumping structure for table labmanagement.approval_permission
CREATE TABLE IF NOT EXISTS `approval_permission` (
  `permission_id` int(11) DEFAULT NULL COMMENT '权限编号',
  `approval_id` int(11) DEFAULT NULL COMMENT '该权限需要处理表单'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table labmanagement.approval_permission: ~5 rows (大约)
/*!40000 ALTER TABLE `approval_permission` DISABLE KEYS */;
INSERT INTO `approval_permission` (`permission_id`, `approval_id`) VALUES
	(3, 1),
	(4, 3),
	(5, 5),
	(6, 7),
	(6, 9);
/*!40000 ALTER TABLE `approval_permission` ENABLE KEYS */;

-- Dumping structure for table labmanagement.approval_status
CREATE TABLE IF NOT EXISTS `approval_status` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `status` varchar(255) NOT NULL COMMENT '状态',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.approval_status: ~12 rows (大约)
/*!40000 ALTER TABLE `approval_status` DISABLE KEYS */;
INSERT INTO `approval_status` (`id`, `status`, `created_at`, `updated_at`) VALUES
	(1, '借用部门负责人待审批', '2020-09-22 07:09:58', NULL),
	(2, '借用部门负责人审批未通过', '2020-09-22 07:09:59', NULL),
	(3, '借用管理员待审批', '2020-09-22 07:09:59', NULL),
	(4, '借用管理员审批未通过', '2020-09-22 07:09:59', NULL),
	(5, '中心主任待审批', '2020-09-22 07:10:00', NULL),
	(6, '中心主任审批未通过', '2020-09-22 07:10:00', NULL),
	(7, '设备出库待确认', '2020-09-22 07:10:01', NULL),
	(8, '设备出库未通过', '2020-09-22 07:10:01', NULL),
	(9, '设备待归还', '2020-09-22 07:10:02', NULL),
	(10, '设备归还未通过', '2020-09-22 07:10:02', NULL),
	(11, '审批通过/表单完成', '2020-09-22 07:10:03', NULL),
	(12, '用户撤销申请', '2020-09-22 07:10:03', NULL);
/*!40000 ALTER TABLE `approval_status` ENABLE KEYS */;

-- Dumping structure for table labmanagement.center_director
CREATE TABLE IF NOT EXISTS `center_director` (
  `approval_id` int(20) NOT NULL COMMENT '审批id',
  `work_id` int(20) NOT NULL COMMENT '审批人id',
  `status` int(2) NOT NULL COMMENT '审批结果',
  `created_at` timestamp NOT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`approval_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.center_director: ~0 rows (大约)
/*!40000 ALTER TABLE `center_director` DISABLE KEYS */;
/*!40000 ALTER TABLE `center_director` ENABLE KEYS */;

-- Dumping structure for table labmanagement.check_info
CREATE TABLE IF NOT EXISTS `check_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `inspection_id` varchar(20) NOT NULL COMMENT '检查表id',
  `lab_id` int(20) NOT NULL COMMENT '实验室编号',
  `lab_name` varchar(255) NOT NULL COMMENT '实验室名称',
  `class_name` varchar(255) NOT NULL COMMENT '课程名称',
  `teacher_name` varchar(255) NOT NULL COMMENT '任课教师',
  `teaching_operation` varchar(255) NOT NULL COMMENT '教学运行情况',
  `open_lab_operation` varchar(255) NOT NULL COMMENT '开放实验室运行情况',
  `note` varchar(40) NOT NULL COMMENT '备注',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.check_info: ~0 rows (大约)
/*!40000 ALTER TABLE `check_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `check_info` ENABLE KEYS */;

-- Dumping structure for table labmanagement.email_check
CREATE TABLE IF NOT EXISTS `email_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `work_id` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table labmanagement.email_check: ~0 rows (大约)
/*!40000 ALTER TABLE `email_check` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_check` ENABLE KEYS */;

-- Dumping structure for table labmanagement.equipment
CREATE TABLE IF NOT EXISTS `equipment` (
  `equipment_id` int(20) NOT NULL COMMENT '主键',
  `equipment_name` varchar(255) NOT NULL COMMENT '设备名称',
  `equipment_model` varchar(255) NOT NULL COMMENT '设备编号',
  `attachment` varchar(255) NOT NULL COMMENT '附件',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`equipment_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.equipment: ~0 rows (大约)
/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;

-- Dumping structure for table labmanagement.equipment_acceptance
CREATE TABLE IF NOT EXISTS `equipment_acceptance` (
  `approval_id` int(20) NOT NULL COMMENT '审批id',
  `work_id` int(20) NOT NULL COMMENT '审批人id',
  `status` int(2) NOT NULL COMMENT '审批结果',
  `created_at` timestamp NOT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`approval_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.equipment_acceptance: ~0 rows (大约)
/*!40000 ALTER TABLE `equipment_acceptance` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipment_acceptance` ENABLE KEYS */;

-- Dumping structure for table labmanagement.equipment_borrow
CREATE TABLE IF NOT EXISTS `equipment_borrow` (
  `form_id` varchar(20) NOT NULL COMMENT '设备借用id',
  `useinfo` text COMMENT '借用用途',
  `department` varchar(255) NOT NULL COMMENT '借用部门',
  `start_time` timestamp NOT NULL COMMENT '申请借用时间',
  `expect_time` timestamp NULL DEFAULT NULL COMMENT '预计归还时间',
  `borrow_name` varchar(255) DEFAULT NULL COMMENT '借用人',
  `phone` int(11) DEFAULT NULL COMMENT '电话',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`form_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.equipment_borrow: ~1 rows (大约)
/*!40000 ALTER TABLE `equipment_borrow` DISABLE KEYS */;
INSERT INTO `equipment_borrow` (`form_id`, `useinfo`, `department`, `start_time`, `expect_time`, `borrow_name`, `phone`, `created_at`, `updated_at`) VALUES
	('C2010225258', 'xxxx', 'xxx', '2020-09-23 14:47:13', '2020-09-23 14:47:15', 'TH', 1231231231, '2020-09-23 14:47:28', NULL);
/*!40000 ALTER TABLE `equipment_borrow` ENABLE KEYS */;

-- Dumping structure for table labmanagement.equipment_borrow_list
CREATE TABLE IF NOT EXISTS `equipment_borrow_list` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `equipment_borrow_id` varchar(20) NOT NULL COMMENT '设备借用表id',
  `equipment_name` varchar(255) NOT NULL COMMENT '设备名称',
  `equipment_model` varchar(255) NOT NULL COMMENT '设备编号',
  `number` int(10) NOT NULL COMMENT '数量',
  `attachment` varchar(255) DEFAULT NULL COMMENT '附件',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.equipment_borrow_list: ~2 rows (大约)
/*!40000 ALTER TABLE `equipment_borrow_list` DISABLE KEYS */;
INSERT INTO `equipment_borrow_list` (`id`, `equipment_borrow_id`, `equipment_name`, `equipment_model`, `number`, `attachment`, `created_at`, `updated_at`) VALUES
	(1, 'C2010225258', 'xx', 'xx', 1, '111', '2020-09-23 14:48:15', NULL),
	(2, 'C2010225258', '12x', '12x', 1, '111', '2020-09-23 15:04:09', NULL);
/*!40000 ALTER TABLE `equipment_borrow_list` ENABLE KEYS */;

-- Dumping structure for table labmanagement.equipment_manager
CREATE TABLE IF NOT EXISTS `equipment_manager` (
  `approval_id` int(20) NOT NULL COMMENT '审批id',
  `work_id` int(20) NOT NULL COMMENT '审批人id',
  `status` int(2) NOT NULL COMMENT '审批结果',
  `created_at` timestamp NOT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`approval_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.equipment_manager: ~0 rows (大约)
/*!40000 ALTER TABLE `equipment_manager` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipment_manager` ENABLE KEYS */;

-- Dumping structure for table labmanagement.final_laboratory_teaching_inspection
CREATE TABLE IF NOT EXISTS `final_laboratory_teaching_inspection` (
  `form_id` varchar(20) NOT NULL COMMENT '主键',
  `recorder_name` varchar(255) NOT NULL COMMENT '记录人姓名',
  `recorder_id` int(20) NOT NULL COMMENT '记录人编号',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`form_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.final_laboratory_teaching_inspection: ~0 rows (大约)
/*!40000 ALTER TABLE `final_laboratory_teaching_inspection` DISABLE KEYS */;
/*!40000 ALTER TABLE `final_laboratory_teaching_inspection` ENABLE KEYS */;

-- Dumping structure for table labmanagement.form
CREATE TABLE IF NOT EXISTS `form` (
  `form_id` varchar(20) NOT NULL COMMENT '主键',
  `work_id` varchar(20) NOT NULL DEFAULT '' COMMENT '提交人编号',
  `form_type_id` int(20) NOT NULL COMMENT '表单种类',
  `created_at` timestamp NOT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`form_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.form: ~4 rows (大约)
/*!40000 ALTER TABLE `form` DISABLE KEYS */;
INSERT INTO `form` (`form_id`, `work_id`, `form_type_id`, `created_at`, `updated_at`) VALUES
	('A2009225256', '123123', 1, '2020-09-22 20:38:19', NULL),
	('A2009225257', '10086', 1, '2020-09-22 21:06:49', NULL),
	('C2010225258', '123123', 3, '2020-09-23 14:40:54', NULL),
	('E2009126765', '123123', 5, '2020-09-23 15:20:53', NULL);
/*!40000 ALTER TABLE `form` ENABLE KEYS */;

-- Dumping structure for table labmanagement.form_type
CREATE TABLE IF NOT EXISTS `form_type` (
  `form_type_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '表单种类id（主键）',
  `form_type` varchar(255) NOT NULL COMMENT '表单种类',
  `form_table` varchar(255) NOT NULL COMMENT '表单对应数据表',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`form_type_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.form_type: ~5 rows (大约)
/*!40000 ALTER TABLE `form_type` DISABLE KEYS */;
INSERT INTO `form_type` (`form_type_id`, `form_type`, `form_table`, `created_at`, `updated_at`) VALUES
	(1, '实验室借用申请表单', 'lab_borrowing', '2020-09-22 06:57:13', NULL),
	(2, '期末实验教学检查记录表', 'final_laboratory_teaching_inspection', '2020-09-22 06:57:14', NULL),
	(3, '实验室仪器设备借用单', 'equipment_borrow', '2020-09-22 06:57:15', NULL),
	(4, '实验室运行记录', 'lab_operation_records', '2020-09-22 06:57:16', NULL),
	(5, '开放实验室使用申请单', 'open_laboratory', '2020-09-22 06:57:17', NULL);
/*!40000 ALTER TABLE `form_type` ENABLE KEYS */;

-- Dumping structure for table labmanagement.head_borrow
CREATE TABLE IF NOT EXISTS `head_borrow` (
  `approval_id` int(20) NOT NULL COMMENT '审批id',
  `work_id` int(20) NOT NULL COMMENT '审批人id',
  `status` int(2) NOT NULL COMMENT '审批结果',
  `created_id` timestamp NOT NULL COMMENT '创建时间',
  `updated_id` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`approval_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.head_borrow: ~0 rows (大约)
/*!40000 ALTER TABLE `head_borrow` DISABLE KEYS */;
/*!40000 ALTER TABLE `head_borrow` ENABLE KEYS */;

-- Dumping structure for table labmanagement.lab_borrowing
CREATE TABLE IF NOT EXISTS `lab_borrowing` (
  `form_id` varchar(20) NOT NULL COMMENT '主键',
  `date` timestamp NOT NULL COMMENT '填报时间',
  `lab_name` varchar(255) NOT NULL COMMENT '实验室名称',
  `lab_id` int(20) NOT NULL COMMENT '实验室编号',
  `class_name` varchar(255) NOT NULL COMMENT '课程名称',
  `class` varchar(255) NOT NULL COMMENT '班级',
  `number` int(20) NOT NULL COMMENT '人数',
  `laboratory_purpose` varchar(255) NOT NULL COMMENT '实验目的',
  `start_time` timestamp NOT NULL COMMENT '开始使用时间',
  `end_time` timestamp NOT NULL COMMENT '使用结束时间',
  `start_class` int(20) NOT NULL COMMENT '开始（第几节课）',
  `end_class` int(20) NOT NULL COMMENT '结束（第几节课）',
  `teacher_name` varchar(20) NOT NULL COMMENT '指导老师',
  `phone` int(11) NOT NULL COMMENT '电话',
  `created_at` timestamp NOT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`form_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.lab_borrowing: ~1 rows (大约)
/*!40000 ALTER TABLE `lab_borrowing` DISABLE KEYS */;
INSERT INTO `lab_borrowing` (`form_id`, `date`, `lab_name`, `lab_id`, `class_name`, `class`, `number`, `laboratory_purpose`, `start_time`, `end_time`, `start_class`, `end_class`, `teacher_name`, `phone`, `created_at`, `updated_at`) VALUES
	('E2009225256', '2020-09-23 10:26:11', '1', 1, '1', '1', 1, '1', '2020-09-23 10:26:20', '2020-09-23 10:26:23', 1, 5, '1', 123123123, '2020-09-23 10:26:39', NULL);
/*!40000 ALTER TABLE `lab_borrowing` ENABLE KEYS */;

-- Dumping structure for table labmanagement.lab_operation_records
CREATE TABLE IF NOT EXISTS `lab_operation_records` (
  `form_id` varchar(20) NOT NULL COMMENT '主键',
  `work_id` int(20) NOT NULL COMMENT '填写人工号/学号',
  `weeks` int(20) NOT NULL COMMENT '周次',
  `professional_classes` varchar(255) NOT NULL COMMENT '专业班级',
  `student_name` varchar(255) NOT NULL COMMENT '学生姓名',
  `number` int(20) NOT NULL COMMENT '人数',
  `class_name` varchar(20) NOT NULL COMMENT '课程名称',
  `class_type` varchar(255) NOT NULL COMMENT '课程类型',
  `teacher_name` varchar(255) NOT NULL COMMENT '任课教师',
  `device_run_condition` varchar(255) NOT NULL COMMENT '设备运行情况',
  `note` varchar(255) NOT NULL COMMENT '备注',
  `created_at` timestamp NOT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`form_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.lab_operation_records: ~0 rows (大约)
/*!40000 ALTER TABLE `lab_operation_records` DISABLE KEYS */;
/*!40000 ALTER TABLE `lab_operation_records` ENABLE KEYS */;

-- Dumping structure for table labmanagement.manager_borrow
CREATE TABLE IF NOT EXISTS `manager_borrow` (
  `approval_id` int(20) NOT NULL,
  `work_id` int(20) NOT NULL COMMENT '审批人id',
  `status` int(2) NOT NULL COMMENT '审批结果',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`approval_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.manager_borrow: ~0 rows (大约)
/*!40000 ALTER TABLE `manager_borrow` DISABLE KEYS */;
/*!40000 ALTER TABLE `manager_borrow` ENABLE KEYS */;

-- Dumping structure for table labmanagement.open_laboratory
CREATE TABLE IF NOT EXISTS `open_laboratory` (
  `form_id` varchar(20) NOT NULL COMMENT '主键',
  `use_reason` text NOT NULL COMMENT '使用原因',
  `project_name` varchar(255) NOT NULL COMMENT '项目名称',
  `start_time` timestamp NOT NULL COMMENT '开始使用时间',
  `end_time` timestamp NOT NULL COMMENT '使用结束时间',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`form_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.open_laboratory: ~1 rows (大约)
/*!40000 ALTER TABLE `open_laboratory` DISABLE KEYS */;
INSERT INTO `open_laboratory` (`form_id`, `use_reason`, `project_name`, `start_time`, `end_time`, `created_at`, `updated_at`) VALUES
	('E2009126765', 'xx', 'xxx', '2020-09-23 15:21:18', '2020-09-29 15:21:19', '2020-09-23 15:21:32', NULL);
/*!40000 ALTER TABLE `open_laboratory` ENABLE KEYS */;

-- Dumping structure for table labmanagement.open_laboratory_student
CREATE TABLE IF NOT EXISTS `open_laboratory_student` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `open_laboratory_id` varchar(20) NOT NULL COMMENT '开放实验室使用表id',
  `student_name` varchar(255) NOT NULL COMMENT '学生姓名',
  `student_id` varchar(20) NOT NULL DEFAULT '' COMMENT '学号',
  `student_phone` varchar(20) NOT NULL DEFAULT '' COMMENT '电话',
  `take_work` varchar(255) NOT NULL COMMENT '承担的工作',
  `applicant_name` varchar(255) NOT NULL COMMENT '申请人名称',
  `date` timestamp NOT NULL COMMENT '日期',
  `created_at` timestamp NOT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.open_laboratory_student: ~2 rows (大约)
/*!40000 ALTER TABLE `open_laboratory_student` DISABLE KEYS */;
INSERT INTO `open_laboratory_student` (`id`, `open_laboratory_id`, `student_name`, `student_id`, `student_phone`, `take_work`, `applicant_name`, `date`, `created_at`, `updated_at`) VALUES
	(1, 'E2009126765', 'TH', '12121', '12121', '1', '2121', '2020-09-23 15:26:24', '2020-09-23 15:26:26', NULL),
	(2, 'E2009126765', 'LJH', '12121', '212121', '2', '2121', '2020-09-23 15:26:56', '2020-09-23 15:26:57', NULL);
/*!40000 ALTER TABLE `open_laboratory_student` ENABLE KEYS */;

-- Dumping structure for table labmanagement.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `permission_id` int(2) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` varchar(40) NOT NULL COMMENT '权限职位',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`permission_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.permissions: ~8 rows (大约)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`permission_id`, `type`, `created_at`, `updated_at`) VALUES
	(1, '学生', '2020-09-22 06:49:32', NULL),
	(2, '普通老师', '2020-09-22 06:49:34', NULL),
	(3, '借用部门负责人', '2020-09-22 06:49:35', NULL),
	(4, '实验室借用管理员', '2020-09-22 06:49:36', NULL),
	(5, '实验室中心主任', '2020-09-22 06:49:37', NULL),
	(6, '设备管理员', '2020-09-22 06:49:38', NULL),
	(7, '实验室教学记录检查员', '2020-09-22 06:49:39', NULL),
	(999, '超级管理员', '2020-09-22 06:49:40', NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table labmanagement.reasons
CREATE TABLE IF NOT EXISTS `reasons` (
  `approval_id` int(11) NOT NULL DEFAULT '0' COMMENT '审批表id',
  `reason` text NOT NULL COMMENT '原因',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`approval_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.reasons: ~3 rows (大约)
/*!40000 ALTER TABLE `reasons` DISABLE KEYS */;
INSERT INTO `reasons` (`approval_id`, `reason`, `created_at`, `updated_at`) VALUES
	(1, 'resonns', '2020-09-23 20:30:03', NULL),
	(2, 'resonns', '2020-09-23 20:30:04', NULL),
	(3, 'xxxxxx', '2020-09-24 08:16:25', '2020-09-24 08:16:25');
/*!40000 ALTER TABLE `reasons` ENABLE KEYS */;

-- Dumping structure for table labmanagement.user
CREATE TABLE IF NOT EXISTS `user` (
  `work_id` varchar(20) NOT NULL DEFAULT '' COMMENT '工号/学号（主键）',
  `password` text NOT NULL COMMENT '密码',
  `permission_id` int(2) DEFAULT NULL COMMENT '权限',
  `status` int(1) DEFAULT NULL COMMENT '状态',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`work_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.user: ~2 rows (大约)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`work_id`, `password`, `permission_id`, `status`, `created_at`, `updated_at`) VALUES
	('10086', '123456', 3, 1, '2020-09-22 20:39:35', NULL),
	('123123', '123123', 1, 1, '2020-09-23 10:20:25', NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table labmanagement.user_info
CREATE TABLE IF NOT EXISTS `user_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` varchar(50) NOT NULL DEFAULT '' COMMENT '用户表id',
  `name` varchar(40) NOT NULL,
  `phone` varchar(20) NOT NULL COMMENT '电话',
  `email` varchar(40) NOT NULL COMMENT '邮箱',
  `created_at` timestamp NOT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table labmanagement.user_info: ~2 rows (大约)
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` (`id`, `user_id`, `name`, `phone`, `email`, `created_at`, `updated_at`) VALUES
	(1, '10086', '汤海', '10010001000', '100@example.com', '2020-09-22 20:41:31', NULL),
	(2, '123123', 'TH', '100100010000', 'xx@example.com', '2020-09-23 11:21:01', NULL);
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
