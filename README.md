本项目是成都东软学院实验实训中心OA系统，系统设计的主要目的是帮助各学院老师完成实验室场地借用、实验室仪器借用、实验室教学检查记录、实验室运行记录、开放实验室申请等表格的在线申请、审批，目前系统包含：超级管理员、普通管理员、组织负责人（学生负责人）、个人信息管理等模块。用户可以根据自己账号的等级进行表单的申请或者审批，完成相应业务需求。

此仓库包含：  
1、实训实验室管理系统数据库设计图：
![](https://gitee.com/ouyang_shenglin/blogImage/raw/master/img/实训实验室管理系统数据库设计.jpg)
2、实训实验室管理系统需求分析图：
![](https://gitee.com/ouyang_shenglin/blogImage/raw/master/img/实训实验室管理系统需求分析.jpg)
3、实训实验室管理系统原型图：
![](https://gitee.com/ouyang_shenglin/blogImage/raw/master/img/20220301170912.png)

